package edu.fatec.sge.models;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "grades")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id", scope=Grade.class)
public class Grade {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "course_discipline_id", insertable = false, updatable = false)
    private Long courseDisciplineId;
    @Column(name = "student_id", insertable = false, updatable = false)
    private Long studentId;

    @NotNull
    @Column(name = "exam_one")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "10.0")
    private BigDecimal examOne;

    @Column(name = "exam_two")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "10.0")
    private BigDecimal examTwo;

    @ManyToOne
    @JoinColumn(name="student_id")
    private Student student;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "course_discipline_id")
    private CourseDiscipline courseDiscipline;
}
