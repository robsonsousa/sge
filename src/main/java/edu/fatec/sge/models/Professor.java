package edu.fatec.sge.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@ApiModel
@Entity
@Table(name = "professors")
@PrimaryKeyJoinColumn(name="account_id")
@OnDelete(action = OnDeleteAction.CASCADE)
public class Professor extends Account {
    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "professor_id")
    private Set<CourseDiscipline> courseDisciplines;
}
