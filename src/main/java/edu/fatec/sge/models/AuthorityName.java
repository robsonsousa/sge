package edu.fatec.sge.models;

public enum AuthorityName {
    ROLE_STUDENT, ROLE_PROFESSOR, ROLE_ADMIN
}