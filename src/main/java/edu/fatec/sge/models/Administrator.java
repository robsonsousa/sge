package edu.fatec.sge.models;

import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@ApiModel
@Entity
@Table(name = "administrators")
@PrimaryKeyJoinColumn(name="account_id")
@DynamicUpdate
public class Administrator extends Account {
}
