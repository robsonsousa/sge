package edu.fatec.sge.models;

import com.fasterxml.jackson.annotation.*;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
@ApiModel
@Entity
@Table(name = "students")
@PrimaryKeyJoinColumn(name="account_id")
@OnDelete(action = OnDeleteAction.CASCADE)
public class Student extends Account {
    @NotNull
    @Size(min = 10, max = 10)
    @Pattern(regexp = "[0-9]{7}[a-zA-Z]{3}")
    @Column(unique = true)
    private String ra;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "student_disciplines",
            joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "course_discipline_id", referencedColumnName = "id"))
    private Set<CourseDiscipline> courseDisciplines;

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "student_id")
    private Set<Grade> grades;

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "student_id")
    private Set<Presence> presences;
}
