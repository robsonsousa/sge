package edu.fatec.sge.repositories;

import edu.fatec.sge.models.Course;
import edu.fatec.sge.models.CourseDiscipline;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CourseDisciplineRepository extends CrudRepository<CourseDiscipline, Long> {
    List<CourseDiscipline> findAll();

    CourseDiscipline findById(Long id);

    @Query("select cd from CourseDiscipline cd WHERE cd.courseId = :courseId AND cd.disciplineId = :disciplineId")
    List<CourseDiscipline> findByCourseAndDiscipline(@Param("courseId") Long courseId,
                                               @Param("disciplineId") Long disciplineId);
}
