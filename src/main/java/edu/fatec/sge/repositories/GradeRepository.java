package edu.fatec.sge.repositories;

import edu.fatec.sge.models.Grade;
import edu.fatec.sge.models.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GradeRepository extends CrudRepository<Grade, Long> {
    Grade findByStudent(Student student);

    List<Grade> findAll();

    Grade findById(Long id);

    @Query("SELECT g FROM Grade g JOIN g.courseDiscipline cd WHERE cd.disciplineId = :disciplineId AND cd.professorId = :professorId")
    List<Grade> findAllStudentsByDiscipline(@Param("disciplineId") Long disciplineId,
                                           @Param("professorId") Long ProfessorId);

    @Query("SELECT g FROM Grade g JOIN g.courseDiscipline cd WHERE g.studentId = :studentId AND cd.disciplineId = :disciplineId AND cd.professorId = :professorId")
    List<Grade> findByStudentAndDiscipline(@Param("disciplineId") Long disciplineId,
                                           @Param("studentId") Long studentId,
                                           @Param("professorId") Long ProfessorId);


}