package edu.fatec.sge.repositories;

import edu.fatec.sge.models.Discipline;
import edu.fatec.sge.models.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends CrudRepository<Student, Long> {
    List<Student> findAll();
    Student findById(Long id);
    Student findByUsername(String username);


    @Query("SELECT s FROM Student s JOIN s.courseDisciplines cd WHERE cd.courseId = :courseId AND cd.disciplineId = :disciplineId AND cd.professorId = :professorId")
    List<Student> findAllByDisciplineProfessor(@Param("courseId") Long courseId,
                                                           @Param("disciplineId") Long disciplineId,
                                                           @Param("professorId") Long professorId);
}