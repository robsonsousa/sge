package edu.fatec.sge.security;

import edu.fatec.sge.models.Account;
import edu.fatec.sge.models.Authority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

public class JWTAccountFactory {
    private JWTAccountFactory() {
    }

    public static JWTAccount create(Account account) {
        return new JWTAccount(
                account.getId(),
                account.getUsername(),
                account.getName(),
                account.getEmail(),
                account.getPassword(),
                mapToGrantedAuthorities(account.getAuthorities()),
                account.getLastPasswordResetDate()
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<Authority> authorities) {
        return authorities.stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getName().name()))
                .collect(Collectors.toList());
    }
}
