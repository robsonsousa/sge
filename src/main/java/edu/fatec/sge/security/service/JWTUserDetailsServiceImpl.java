package edu.fatec.sge.security.service;

import edu.fatec.sge.models.Account;
import edu.fatec.sge.repositories.AccountRepository;
import edu.fatec.sge.security.JWTAccountFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by stephan on 20.03.16.
 */
@Service
public class JWTUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findByUsername(username);

        if (account == null) {
            throw new UsernameNotFoundException(String.format("No account found with username '%s'.", username));
        } else {
            return JWTAccountFactory.create(account);
        }
    }
}