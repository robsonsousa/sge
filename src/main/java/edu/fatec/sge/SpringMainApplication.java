package edu.fatec.sge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@EnableAutoConfiguration(exclude = LiquibaseAutoConfiguration.class)
public class SpringMainApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringMainApplication.class, args);
	}
}