package edu.fatec.sge.rest.professor;

import edu.fatec.sge.models.*;
import edu.fatec.sge.repositories.CourseRepository;
import edu.fatec.sge.repositories.DisciplineRepository;
import edu.fatec.sge.repositories.ScheduleRepository;
import edu.fatec.sge.repositories.StudentRepository;
import edu.fatec.sge.security.JWTAccount;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController("ProfessorCourseController")
@RequestMapping("/professor/courses/")
public class CourseController {
    private ScheduleRepository scheduleRepository;
    private DisciplineRepository disciplineRepository;
    private CourseRepository courseRepository;
    private StudentRepository studentRepository;

    @Autowired
    public CourseController(ScheduleRepository scheduleRepository,
                            DisciplineRepository disciplineRepository,
                            CourseRepository courseRepository,
                            StudentRepository studentRepository
                            ) {
        this.scheduleRepository = scheduleRepository;
        this.disciplineRepository = disciplineRepository;
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
    }

    // GET: /professor/courses/
    @ApiOperation(value = "Get courses", response = Discipline.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully retrieved courses"),
            @ApiResponse(code = 404, message = "The resource was not found") })
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Course>> show() {
        JWTAccount professor = (JWTAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new ResponseEntity<>(
                courseRepository.findAllByProfessor(professor.getId()),
                HttpStatus.OK
        );
    }

    // GET: /professor/courses/:id/disciplines/
    @ApiOperation(value = "Get disciplines from course", response = Discipline.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully retrieved disciplines"),
            @ApiResponse(code = 404, message = "The resource was not found") })
    @RequestMapping(value = "/{courseId}/disciplines/", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Discipline>> showDisciplines(@PathVariable Long courseId) {
        JWTAccount professor = (JWTAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new ResponseEntity<>(
                disciplineRepository.findAllByCourseIdAndProfessorId(courseId, professor.getId()),
                HttpStatus.OK
        );
    }

    // GET: /professor/courses/:id/disciplines/:id
    @ApiOperation(value = "Get discipline ID from course", response = Discipline.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully retrieved discipline."),
            @ApiResponse(code = 404, message = "The resource was not found") })
    @RequestMapping(value = "/{courseId}/disciplines/{disciplineId}", method = RequestMethod.GET)
    public ResponseEntity<Discipline> showDiscipline(@PathVariable Long courseId, @PathVariable Long disciplineId) {
        JWTAccount professor = (JWTAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Discipline discipline = disciplineRepository.findByCourseIdAndDisciplineIdAndProfessorId(courseId, disciplineId, professor.getId());
        return (discipline != null) ? new ResponseEntity<>(discipline, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // GET: /professor/courses/:id/disciplines/:id/students/
    @ApiOperation(value = "Get students from course discipline", response = Discipline.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully retrieved disciplines."),
            @ApiResponse(code = 404, message = "The resource was not found") })
    @RequestMapping(value = "/{courseId}/disciplines/{disciplineId}/students/", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Student>> showStudentsDiscipline(@PathVariable Long courseId, @PathVariable Long disciplineId) {
        JWTAccount professor = (JWTAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new ResponseEntity<>(
                studentRepository.findAllByDisciplineProfessor(courseId, disciplineId, professor.getId()),
                HttpStatus.OK
        );
    }

    // GET: /professor/courses/:id/disciplines/:id/schedules
    @ApiOperation(value = "Get schedules from course discipline", response = Discipline.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully retrieved schedules"),
            @ApiResponse(code = 404, message = "The resource was not found") })
    @RequestMapping(value = "/{courseId}/disciplines/{disciplineId}/schedules", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Schedule>> showDisciplineSchedules(@PathVariable Long courseId, @PathVariable Long disciplineId) {
        JWTAccount professor = (JWTAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new ResponseEntity<>(
                scheduleRepository.findAllByCourseAndDisciplineAndProfessor(courseId, disciplineId, professor.getId()),
                HttpStatus.OK
        );
    }

}

