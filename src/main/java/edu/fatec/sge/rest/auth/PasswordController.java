package edu.fatec.sge.rest.auth;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import edu.fatec.sge.models.Account;
import edu.fatec.sge.models.Student;
import edu.fatec.sge.repositories.AccountRepository;
import freemarker.template.TemplateException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

@RestController("AuthPasswordController")
@RequestMapping("/auth/recover/")
public class PasswordController {

    private AccountRepository accountRepository;

    @Autowired
    public PasswordController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @ApiOperation(value = "Recover password of a System User", response = Student.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully recovered the password.") })
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Void> recoverPassword(@RequestBody String jsonString) {
        JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();

        if (jsonObject.get("cpf") != null && jsonObject.get("birthday") != null) {
            String cpf = jsonObject.get("cpf").getAsString();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

            LocalDate birthday = LocalDate.parse(jsonObject.get("birthday").getAsString(), formatter);

            Account account = accountRepository.findByCpfAndBirthday(cpf, birthday);

            if (account == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            Properties props = new Properties();

            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");

            Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication()
                    {
                        return new PasswordAuthentication("sge.mail.fatec@gmail.com", "fe123456");
                    }
            });

            Message message = new MimeMessage(session);
            try {
                message.setFrom(new InternetAddress("sge.mail.fatec@gmail.com"));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("koblinger@live.com"));
                message.setSubject("[SGE] Recuperação de Senha");

                Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);

                cfg.setClassForTemplateLoading(PasswordController.class, "/templates/");
                cfg.setDefaultEncoding("UTF-8");
                cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
                Template template = cfg.getTemplate("passwordRecover.ftl");

                //Pass custom param values
                String password = RandomStringUtils.randomAlphanumeric(8).toUpperCase();

                Map<String, String> paramMap = new HashMap<>();
                paramMap.put("name", account.getName());
                paramMap.put("username", account.getUsername());
                paramMap.put("email", account.getEmail());
                paramMap.put("password", password);
                Writer out = new StringWriter();

                template.process(paramMap, out);
                BodyPart body = new MimeBodyPart();
                body.setContent(out.toString(), "text/html; charset=utf-8");
                Multipart multipart = new MimeMultipart();
                multipart.addBodyPart(body);
                message.setContent(multipart);
                Transport.send(message);

                account.setPassword(password);
                accountRepository.save(account);

            } catch (MessagingException | IOException | TemplateException e) {
                e.printStackTrace();
            }

            return new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
