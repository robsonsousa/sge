package edu.fatec.sge.rest.admin;

import edu.fatec.sge.models.Administrator;
import edu.fatec.sge.models.Authority;
import edu.fatec.sge.models.AuthorityName;
import edu.fatec.sge.models.Professor;
import edu.fatec.sge.repositories.ProfessorRepository;
import edu.fatec.sge.utils.NullAwareBeanUtilsBean;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/admin/professors/")
public class ProfessorController {
    private ProfessorRepository professorRepository;

    @Autowired
    public ProfessorController(ProfessorRepository professorRepository) {
        this.professorRepository = professorRepository;
    }

    @ApiOperation(value = "Get one professor base on its Id or Username", response = Administrator.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved professor."),
            @ApiResponse(code = 404, message = "The resource was not found")})
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Professor> show(@PathVariable String id) {
        Professor professor;
        if (NumberUtils.isNumber(id)) {
            professor = professorRepository.findById(Long.parseLong(id));
        } else {
            professor = professorRepository.findByUsername(id);
        }
        return (professor != null) ? new ResponseEntity<>(professor, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Query all professors stored in database", response = Professor.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list") })
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Professor>> index() {
        return new ResponseEntity<>(professorRepository.findAll(), HttpStatus.OK);
    }
    
    @ApiOperation(value = "Create one Professor", response = Professor.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully created Professor") })
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Professor> create(@RequestBody Professor prof) {
        Authority authority = new Authority();
        authority.setName(AuthorityName.ROLE_PROFESSOR);
        authority.setId(2L);
        List<Authority> authorities = new ArrayList<>();
        authorities.add(authority);
        prof.setAuthorities(authorities);

        professorRepository.save(prof);
        return (prof.getId() != 0) ? new ResponseEntity<>(prof, HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @ApiOperation(value = "Remove one Professor by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Professor deleted with success"),
            @ApiResponse(code = 406, message = "Fail to delete the Professor") })
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        try {
            professorRepository.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }
    
    @ApiOperation(value = "Update one Professor", response = Professor.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully updated Professor") })
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Professor> update(@RequestBody Professor prof, @PathVariable Long id) {
        Professor queryProfessor = professorRepository.findById(id);

        BeanUtilsBean notNull = new NullAwareBeanUtilsBean();
        try {
            notNull.copyProperties(queryProfessor, prof);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        professorRepository.save(queryProfessor);

        return (queryProfessor.getId() != 0) ? new ResponseEntity<>(queryProfessor, HttpStatus.ACCEPTED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}


