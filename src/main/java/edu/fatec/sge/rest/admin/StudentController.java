package edu.fatec.sge.rest.admin;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import edu.fatec.sge.models.*;
import edu.fatec.sge.repositories.CourseDisciplineRepository;
import edu.fatec.sge.repositories.StudentRepository;
import edu.fatec.sge.utils.NullAwareBeanUtilsBean;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/admin/students/")
public class StudentController {
    private StudentRepository studentRepository;
    private CourseDisciplineRepository courseDisciplineRepository;

    @Autowired
    public StudentController(StudentRepository studentRepository,
                             CourseDisciplineRepository courseDisciplineRepository) {
        this.studentRepository = studentRepository;
        this.courseDisciplineRepository = courseDisciplineRepository;
    }

    @ApiOperation(value = "Get one student base on its Id or Username", response = Administrator.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved student."),
            @ApiResponse(code = 404, message = "The resource was not found")})
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Student> show(@PathVariable String id) {
        Student student;
        if (NumberUtils.isNumber(id)) {
            student = studentRepository.findById(Long.parseLong(id));
        } else {
            student = studentRepository.findByUsername(id);
        }
        return (student != null) ? new ResponseEntity<>(student, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Query all students stored in database", response = Student.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list") })
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Student>> index() {
        return new ResponseEntity<>(studentRepository.findAll(), HttpStatus.OK);
    }
    
    @ApiOperation(value = "Create one Student", response = Student.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully created Student") })
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Student> create(@RequestBody Student stud) {
        Authority authority = new Authority();
        authority.setName(AuthorityName.ROLE_STUDENT);
        authority.setId(2L);
        List<Authority> authorities = new ArrayList<>();
        authorities.add(authority);
        stud.setAuthorities(authorities);

        studentRepository.save(stud);
        return (stud.getId() != 0) ? new ResponseEntity<>(stud, HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Register one Student into a Discipline", response = Student.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully registered Student") })
    @RequestMapping(value = "/disciplines/register", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<CourseDiscipline> registerStudentDiscipline(@RequestBody String jsonString) {
        JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();

        Long studentId = jsonObject.get("studentId").getAsLong();
        Student student = studentRepository.findById(studentId);

        Long courseDisciplineId = jsonObject.get("courseDisciplineId").getAsLong();
        CourseDiscipline courseDiscipline = courseDisciplineRepository.findById(courseDisciplineId);

        Set<CourseDiscipline> courseDisciplineSet = student.getCourseDisciplines();
        courseDisciplineSet.add(courseDiscipline);
        student.setCourseDisciplines(courseDisciplineSet);

        studentRepository.save(student);
        return (courseDiscipline.getId() != 0) ? new ResponseEntity<>(courseDiscipline, HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Update one Student", response = Professor.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully updated Student") })
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Student> update(@RequestBody Student stud, @PathVariable Long id) {
        Student queryStudent = studentRepository.findById(id);

        BeanUtilsBean notNull = new NullAwareBeanUtilsBean();
        try {
            notNull.copyProperties(queryStudent, stud);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        studentRepository.save(queryStudent);

        return (queryStudent.getId() != 0) ? new ResponseEntity<>(queryStudent, HttpStatus.ACCEPTED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @ApiOperation(value = "Remove one Student by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Student deleted with success"),
            @ApiResponse(code = 406, message = "Fail to Student the Professor") })
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        try {
            studentRepository.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }

}


