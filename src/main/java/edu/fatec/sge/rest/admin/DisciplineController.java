package edu.fatec.sge.rest.admin;

import edu.fatec.sge.models.Course;
import edu.fatec.sge.models.Discipline;
import edu.fatec.sge.repositories.CourseRepository;
import edu.fatec.sge.repositories.DisciplineRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/disciplines/")
public class DisciplineController {
    private CourseRepository courseRepository;
    private DisciplineRepository disciplineRepository;

    @Autowired
    public DisciplineController(DisciplineRepository disciplineRepository, CourseRepository courseRepository) {
        this.disciplineRepository = disciplineRepository;
        this.courseRepository = courseRepository;
    }

    // GET: /admin/disciplines/
    @ApiOperation(value = "Query all disciplines stored in database", response = Discipline.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list") })
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Discipline>> index() {
        return new ResponseEntity<>(disciplineRepository.findAll(), HttpStatus.OK);
    }

    // GET: /admin/disciplines/:id
    @ApiOperation(value = "Get one discipline based on its ID", response = Discipline.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully retrieved discipline"),
            @ApiResponse(code = 404, message = "The resource was not found") })
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Discipline> show(@PathVariable Long id) {
        Discipline discipline = disciplineRepository.findById(id);
        return (discipline != null) ? new ResponseEntity<>(discipline, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // GET: /admin/disciplines/:id/courses/
    @ApiOperation(value = "Get courses based on discipline's ID", response = Discipline.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully retrieved discipline"),
            @ApiResponse(code = 404, message = "The resource was not found") })
    @RequestMapping(value = "/{id}/courses", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Course>>  showCourses(@PathVariable Long id) {
        return new ResponseEntity<>(courseRepository.findByDisciplineId(id), HttpStatus.OK);
    }

    // POST: /admin/disciplines/
    @ApiOperation(value = "Create one discipline", response = Discipline.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully created discipline") })
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Discipline> create(@RequestBody Discipline discipline) {
        disciplineRepository.save(discipline);
        return (discipline.getId() != 0) ? new ResponseEntity<>(discipline, HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // PUT: /admin/disciplines/{id}
    @ApiOperation(value = "Update one discipline", response = Discipline.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully updated discipline") })
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Discipline> update(@RequestBody Discipline discipline, @PathVariable Long id) {
        discipline.setId(id);
        disciplineRepository.save(discipline);
        return (discipline.getId() != 0) ? new ResponseEntity<>(discipline, HttpStatus.ACCEPTED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // DELETE: /admin/disciplines/{id}
    @ApiOperation(value = "Remove one Discipline by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Discipline deleted with success"),
            @ApiResponse(code = 406, message = "Fail to delete the discipline") })
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        try {
            disciplineRepository.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }
}