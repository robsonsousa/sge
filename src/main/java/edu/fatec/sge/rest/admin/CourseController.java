package edu.fatec.sge.rest.admin;

import edu.fatec.sge.models.Course;
import edu.fatec.sge.models.CourseDiscipline;
import edu.fatec.sge.models.Discipline;
import edu.fatec.sge.repositories.CourseDisciplineRepository;
import edu.fatec.sge.repositories.CourseRepository;
import edu.fatec.sge.repositories.DisciplineRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("AdminCourseController")
@RequestMapping("/admin/courses/")
public class CourseController {
    private CourseRepository courseRepository;
    private DisciplineRepository disciplineRepository;
    private CourseDisciplineRepository courseDisciplineRepository;

    @Autowired
    public CourseController(DisciplineRepository disciplineRepository,
                            CourseRepository courseRepository,
                            CourseDisciplineRepository courseDisciplineRepository
                            ) {
        this.disciplineRepository = disciplineRepository;
        this.courseRepository = courseRepository;
        this.courseDisciplineRepository = courseDisciplineRepository;
    }
    // GET: /admin/courses/
    @ApiOperation(value = "Query all courses stored in database", response = Course.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list") })
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Course>> index() {
        return new ResponseEntity<>(courseRepository.findAll(), HttpStatus.OK);
    }

    // GET: /admin/courses/:id
    @ApiOperation(value = "Get one course base on its ID", response = Course.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully retrieved course"),
            @ApiResponse(code = 404, message = "The resource was not found") })
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Course> show(@PathVariable Long id) {
        Course course = courseRepository.findById(id);
        return (course != null) ? new ResponseEntity<>(course, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // GET: /admin/courses/:id/disciplines/
    @ApiOperation(value = "Get disciplines based on course's ID", response = Discipline.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully retrieved discipline"),
            @ApiResponse(code = 404, message = "The resource was not found") })
    @RequestMapping(value = "/{id}/disciplines", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Discipline>> showDisciplines(@PathVariable Long id) {
        return new ResponseEntity<>(disciplineRepository.findByCourseId(id), HttpStatus.OK);
    }

    // POST: /admin/courses/:id/disciplines/
    @ApiOperation(value = "Get disciplines based on course's ID", response = Discipline.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully retrieved discipline"),
            @ApiResponse(code = 404, message = "The resource was not found") })
    @RequestMapping(value = "/{id}/disciplines", method = RequestMethod.POST)
    public ResponseEntity<Iterable<Discipline>> addCourseDiscipline(@PathVariable Long id, @RequestBody CourseDiscipline courseDiscipline) {
        Course course = courseRepository.findById(id);
        courseDiscipline.setCourse(course);
        Discipline discipline = disciplineRepository.findById(courseDiscipline.getDisciplineId());
        courseDiscipline.setDiscipline(discipline);

        courseDisciplineRepository.save(courseDiscipline);
        return new ResponseEntity<>(disciplineRepository.findByCourseId(id), HttpStatus.CREATED);
    }

    // DELETE: /admin/courses/:id/disciplines/1
    @ApiOperation(value = "Remove one Course Discipline by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Course Discipline deleted with success"),
            @ApiResponse(code = 406, message = "Fail to delete the course discipline") })
    @DeleteMapping(value = "/{courseId}/disciplines/{disciplineId}")
    public ResponseEntity<Void> deleteCourseDiscipline(@PathVariable Long courseId, @PathVariable Long disciplineId) {
        try {
            List<CourseDiscipline> courseDiscipline = courseDisciplineRepository.findByCourseAndDiscipline(courseId, disciplineId);
            courseDisciplineRepository.delete(courseDiscipline);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    // POST: /admin/courses/
    @ApiOperation(value = "Create one course", response = Course.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully created course") })
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Course> create(@RequestBody Course course) {
        courseRepository.save(course);
        return (course.getId() != 0) ? new ResponseEntity<>(course, HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // PUT: /admin/courses/{id}
    @ApiOperation(value = "Update one course", response = Course.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully updated course") })
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Course> update(@RequestBody Course course, @PathVariable Long id) {
        course.setId(id);
        courseRepository.save(course);
        return (course.getId() != 0) ? new ResponseEntity<>(course, HttpStatus.ACCEPTED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // DELETE: /admin/courses/{id}
    @ApiOperation(value = "Remove one Course by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Course deleted with success"),
            @ApiResponse(code = 406, message = "Fail to delete the course") })
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        try {
            courseRepository.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }
}
