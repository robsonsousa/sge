package unit.models;

import edu.fatec.sge.models.Discipline;
import factories.DisciplineFactory;
import org.junit.Test;
import pl.pojo.tester.api.assertion.Method;
import utils.TestUtil;

import static org.junit.Assert.assertEquals;
import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

public class DisciplineTest extends TestUtil {
    @Test
    public void disciplineTestPojo() {
        final Class<?> classUnderTest = Discipline.class;

        assertPojoMethodsFor(classUnderTest)
                .testing(Method.GETTER)
                .areWellImplemented();
        assertPojoMethodsFor(classUnderTest)
                .testing(Method.SETTER)
                .areWellImplemented();
    }

    @Test
    public void disciplineShouldBeValid() {
        Discipline validDiscipline = DisciplineFactory.validDiscipline();
        assertEquals(0, getErrorSize(validDiscipline));
    }

    @Test
    public void disciplineShouldNotBeValidWithInvalidName() {
        Discipline discipline = DisciplineFactory.validDiscipline();

        discipline.setName(null);
        assertEquals(1, getErrorSize(discipline));
        assertEquals("may not be null", getErrorMessage(discipline));

        discipline.setName("Est");
        assertEquals(1, getErrorSize(discipline));
        assertEquals("size must be between 4 and 100", getErrorMessage(discipline));

        discipline.setName("Banco de Dados 1Banco de Dados 1Banco de Dados 1Banco de Dados 1Banco de Dados 1Banco de Dados 1Banco");
        assertEquals(1, getErrorSize(discipline));
        assertEquals("size must be between 4 and 100", getErrorMessage(discipline));
    }

    @Test
    public void disciplineShouldNotBeValidWithInvalidCode() {
        Discipline discipline = DisciplineFactory.validDiscipline();

        discipline.setCode(null);
        assertEquals(1, getErrorSize(discipline));
        assertEquals("may not be null", getErrorMessage(discipline));

        discipline.setCode("e");
        assertEquals(1, getErrorSize(discipline));
        assertEquals("size must be between 2 and 100", getErrorMessage(discipline));

        discipline.setCode("Banco de Dados 1Banco de Dados 1Banco de Dados 1Banco de Dados 1Banco de Dados 1Banco de Dados 1Banco");
        assertEquals(1, getErrorSize(discipline));
        assertEquals("size must be between 2 and 100", getErrorMessage(discipline));
    }
}
