package unit.models;

import edu.fatec.sge.models.Schedule;
import factories.ScheduleFactory;
import org.junit.Test;
import pl.pojo.tester.api.assertion.Method;
import utils.TestUtil;

import static org.junit.Assert.assertEquals;
import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

public class ScheduleTest extends TestUtil {
    @Test
    public void scheduleTestPojo() {
        final Class<?> classUnderTest = Schedule.class;

        assertPojoMethodsFor(classUnderTest)
                .testing(Method.GETTER)
                .areWellImplemented();
        assertPojoMethodsFor(classUnderTest)
                .testing(Method.SETTER)
                .areWellImplemented();
    }

    @Test
    public void scheduleShouldBeValid() {
        Schedule validSchedule = ScheduleFactory.validSchedule();
        assertEquals( 0, getErrorSize(validSchedule));
    }

    @Test
    public void scheduleShouldNotBeValidWithInvalidTimeStart() {
        Schedule schedule = ScheduleFactory.validSchedule();
        schedule.setTimeStart(null);

        assertEquals( 1, getErrorSize(schedule));
        assertEquals("may not be null", getErrorMessage(schedule));
    }

    @Test
    public void scheduleShouldNotBeValidWithInvalidTimeEnd() {
        Schedule schedule = ScheduleFactory.validSchedule();
        schedule.setTimeEnd(null);

        assertEquals( 1, getErrorSize(schedule));
        assertEquals("may not be null", getErrorMessage(schedule));
    }

    @Test
    public void scheduleShouldNotBeValidWithInvalidQuantity() {
        Schedule schedule = ScheduleFactory.validSchedule();
        schedule.setQuantity(null);

        assertEquals( 1, getErrorSize(schedule));
        assertEquals("may not be null", getErrorMessage(schedule));
    }
}
