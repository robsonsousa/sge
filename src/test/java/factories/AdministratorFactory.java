package factories;

import edu.fatec.sge.models.Administrator;

public class AdministratorFactory {
    public static Administrator validAdministrator() {
        Administrator validAdministrator = new Administrator();
        validAdministrator = (Administrator) AccountFactory.validAccount(validAdministrator);
        validAdministrator.setCpf("14258545082");
        validAdministrator.setRg("309182932X");
        validAdministrator.setEmail("administrator@fatec.com.br");
        validAdministrator.setUsername("administrator");
        validAdministrator.setPassword("1234567891");
        return validAdministrator;
    }
}
