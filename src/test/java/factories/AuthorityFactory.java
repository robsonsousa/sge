package factories;

import edu.fatec.sge.models.Authority;
import edu.fatec.sge.models.AuthorityName;

public class AuthorityFactory {
    public static Authority validAuthority() {
        Authority validAuthority = new Authority();
        validAuthority.setName(AuthorityName.ROLE_ADMIN);
        return validAuthority;
    }
}
