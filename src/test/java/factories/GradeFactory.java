package factories;

import edu.fatec.sge.models.Grade;

import java.math.BigDecimal;

public class GradeFactory {
    public static Grade validGrade() {
        Grade validGrade = new Grade();
        validGrade.setExamOne(new BigDecimal(5.5));
        validGrade.setExamTwo(new BigDecimal(7.0));
        return validGrade;
    }
}
