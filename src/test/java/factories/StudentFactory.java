package factories;

import edu.fatec.sge.models.Authority;
import edu.fatec.sge.models.AuthorityName;
import edu.fatec.sge.models.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentFactory {
    public static Student validStudent() {
        Student validStudent = new Student();
        validStudent = (Student) AccountFactory.validAccount(validStudent);
        validStudent.setRa("1234567BDD");

        Authority authority = new Authority();
        authority.setName(AuthorityName.ROLE_STUDENT);
        authority.setId(2L);
        List<Authority> authorities = new ArrayList<>();
        authorities.add(authority);
        validStudent.setAuthorities(authorities);

        return validStudent;
    }
}
