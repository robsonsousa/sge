package factories;


import edu.fatec.sge.models.CourseDiscipline;

public class CourseDisciplineFactory {

    public static CourseDiscipline validCourseDiscipline() {
        CourseDiscipline validCourseDiscipline = new CourseDiscipline();
        validCourseDiscipline.setSemester(2);
        validCourseDiscipline.setYear(2017);
        validCourseDiscipline.setWorkload(80);

        validCourseDiscipline.setDiscipline(DisciplineFactory.validDiscipline());
        validCourseDiscipline.setCourse(CourseFactory.validCourse());
        return validCourseDiscipline;
    }
}
