package factories;

import edu.fatec.sge.models.Course;

public class CourseFactory {
    public static Course validCourse() {
        Course validCourse = new Course();
        validCourse.setName("Banco de Dados - T.I");
        return validCourse;
    }
}
