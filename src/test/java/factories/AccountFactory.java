package factories;

import edu.fatec.sge.models.Account;
import edu.fatec.sge.models.Authority;
import edu.fatec.sge.models.AuthorityName;
import edu.fatec.sge.models.GenreName;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AccountFactory {
    public static Account validAccount(Account account) {
        account.setEmail("joao@neves.com.br");
        account.setUsername("joaoneves");
        account.setPassword("joao12345");
        account.setName("João das Neves");
        account.setCpf("95554235123");
        account.setRg("3088910X");
        account.setGenre(GenreName.MALE);
        account.setBirthday(LocalDate.now().minusYears(10));

        Authority authority = new Authority();
        authority.setName(AuthorityName.ROLE_ADMIN);
        authority.setId(1L);
        List<Authority> authorities = new ArrayList<>();
        authorities.add(authority);
        account.setAuthorities(authorities);

        return account;
    }
}
