package factories;

import edu.fatec.sge.models.Schedule;
import edu.fatec.sge.models.ScheduleWeekdayName;

import java.sql.Time;

public class ScheduleFactory {
    public static Schedule validSchedule() {
        Schedule validSchedule = new Schedule();
        validSchedule.setWeekday(ScheduleWeekdayName.MONDAY);
        validSchedule.setTimeStart(Time.valueOf("19:35:00"));
        validSchedule.setTimeEnd(Time.valueOf("20:15:00"));
        validSchedule.setQuantity(1);
        return validSchedule;
    }
}
