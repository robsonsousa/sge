package factories;

import java.time.LocalDate;

import edu.fatec.sge.models.Presence;

public class PresenceFactory {
	public static Presence validPresence() {
		Presence validPresence = new Presence();
		validPresence.setDate(LocalDate.now());
		validPresence.setId(1L);
		return validPresence;
	}

}
