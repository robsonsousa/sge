package integration.rest;

import com.google.gson.JsonObject;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;
import utils.IntegrationTestUtil;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;

@Transactional
public class AdminDisciplineControllerTest extends IntegrationTestUtil {
    @Before
    public void setToken() {
        this.setToken("ROLE_ADMIN");
    }

    /*
     * GET: "/admin/disciplines"
     */
    @Test
    public void
    shouldReturn200AndListOfDisciplinesWhenProperlyLogged() {
        given()
            .header("Authorization", "Bearer " + this.getToken())
        .when().
            get("/admin/disciplines/")
        .then()
            .statusCode(200)
            .body("list.size()", greaterThan(0));
    }

    @Test public void
    shouldReturn401WhenRequestListOfDisciplinesWithoutLogin() {
        when()
            .get("/admin/disciplines")
        .then()
            .statusCode(401);
    }

    /*
     * GET: "/admin/disciplines/1"
     */
    @Test
    public void
    shouldReturn200AndOneDisciplineWhenProperlyLogged() {
        given()
            .header("Authorization", "Bearer " + this.getToken())
        .when().
            get("/admin/disciplines/1")
        .then()
            .statusCode(200);
    }
    @Test public void
    shouldReturn401WhenRequestOneDisciplineWithoutLogin() {
        when()
            .get("/admin/disciplines/1")
        .then()
            .statusCode(401);
    }

    /*
     * POST: "/admin/disciplines/"
     */
    @Test
    public void
    shouldReturn201AndCreateOneDisciplineWhenProperlyLogged() {
        JsonObject discipline = new JsonObject();
        discipline.addProperty("name", "Otimização de Banco de Dados");
        discipline.addProperty("code", "OBD");

        Response r =
            given()
                .header("Authorization", "Bearer " + this.getToken())
                .contentType("application/json")
                .body(discipline.toString())
            .when()
                .post("/admin/disciplines/");

        /* Delete after creation */
        int id = r.jsonPath().getInt("id");
        given().header("Authorization", "Bearer " + this.getToken())
                .when().delete("/admin/disciplines/" + id).then().statusCode(200);

        assertEquals(201, r.statusCode());
    }
    @Test public void
    shouldReturn401WhenRequestCreateOneDisciplineWithoutLogin() {
        JsonObject discipline = new JsonObject();
        discipline.addProperty("name", "Gestão da Produção");

        given()
            .contentType("application/json")
            .body(discipline.toString())
        .when()
            .post("/admin/disciplines/")
        .then()
            .statusCode(401);;
    }

    @Test public void
    shouldReturn401WhenRequestDeleteOneDisciplineWithoutLogin() {
        when()
            .delete("/admin/disciplines/1")
        .then()
            .statusCode(401);;
    }

}