package integration.rest;

import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;
import utils.IntegrationTestUtil;

import static org.junit.Assert.assertEquals;

@Transactional
public class ProfessorCourseControllerTest extends IntegrationTestUtil {
    @Before
    public void setToken() {
        this.setToken("ROLE_PROFESSOR");
    }

    @Test
    public void
    placeholderTest() {
        assertEquals(true, true);
    }
    // GET: /professor/courses/:id/disciplines/
    // GET: /professor/courses/:id/disciplines/:id
    // GET: /professor/courses/:id/disciplines/:id/schedules

}
