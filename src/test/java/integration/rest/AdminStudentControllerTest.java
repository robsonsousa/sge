package integration.rest;

import com.google.gson.JsonObject;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;
import utils.IntegrationTestUtil;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;

@Transactional
public class AdminStudentControllerTest extends IntegrationTestUtil {
    @Before
    public void setToken() {
        this.setToken("ROLE_ADMIN");
    }

    /*
     * GET: "/admin/students"
     */
    @Test
    public void
    shouldReturn200AndListOfStudentsWhenProperlyLogged() {
        given()
            .header("Authorization", "Bearer " + this.getToken())
        .when().
            get("/admin/students/")
        .then()
            .statusCode(200)
            .body("list.size()", greaterThan(0));
    }

    @Test public void
    shouldReturn401WhenRequestListOfStudentsWithoutLogin() {
        when()
            .get("/admin/students")
        .then()
            .statusCode(401);
    }

    /*
     * GET: "/admin/students/1"
     */
//    @Test
//    public void
//    shouldReturn200AndOneStudentWhenProperlyLogged() {
//        given()
//            .header("Authorization", "Bearer " + this.getToken())
//        .when().
//            get("/admin/students/1")
//        .then()
//            .statusCode(200);
//    }
//    @Test public void
//    shouldReturn401WhenRequestOneStudentWithoutLogin() {
//        when()
//            .get("/admin/students/1")
//        .then()
//            .statusCode(401);
//    }

    /*
     * POST: "/admin/students/"
     */
//    @Test
//    public void
//    shouldReturn201AndCreateOneStudentWhenProperlyLogged() {
//        JsonObject student = new JsonObject();
//        student.addProperty("name", "Otimização de Banco de Dados");
//        student.addProperty("code", "OBD");
//
//        Response r =
//            given()
//                .header("Authorization", "Bearer " + this.getToken())
//                .contentType("application/json")
//                .body(student.toString())
//            .when()
//                .post("/admin/students/");
//
//        /* Delete after creation */
//        int id = r.jsonPath().getInt("id");
//        given().header("Authorization", "Bearer " + this.getToken())
//                .when().delete("/admin/students/" + id).then().statusCode(200);
//
//        assertEquals(201, r.statusCode());
//    }
    @Test public void
    shouldReturn401WhenRequestCreateOneStudentWithoutLogin() {
        JsonObject student = new JsonObject();
        student.addProperty("name", "Gestão da Produção");

        given()
            .contentType("application/json")
            .body(student.toString())
        .when()
            .post("/admin/students/")
        .then()
            .statusCode(401);;
    }

    @Test public void
    shouldReturn401WhenRequestDeleteOneStudentWithoutLogin() {
        when()
            .delete("/admin/students/1")
        .then()
            .statusCode(401);;
    }

}