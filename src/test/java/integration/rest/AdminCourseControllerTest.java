package integration.rest;

import com.google.gson.JsonObject;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;
import utils.IntegrationTestUtil;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

@Transactional
public class AdminCourseControllerTest extends IntegrationTestUtil {
    @Before
    public void setToken() {
        this.setToken("ROLE_ADMIN");
    }

    /*
     * GET: "/admin/courses"
     */
    @Test
    public void
    shouldReturn200AndListOfCoursesWhenProperlyLogged() {
        given()
            .header("Authorization", "Bearer " + this.getToken())
        .when().
            get("/admin/courses/")
        .then()
            .statusCode(200)
            .body("list.size()", greaterThan(0));
    }

    @Test public void
    shouldReturn401WhenRequestListOfCoursesWithoutLogin() {
        when()
            .get("/admin/courses")
        .then()
            .statusCode(401);
    }

    /*
     * GET: "/admin/courses/1"
     */
    @Test
    public void
    shouldReturn200AndOneCourseWhenProperlyLogged() {
        given()
            .header("Authorization", "Bearer " + this.getToken())
        .when().
            get("/admin/courses/1")
        .then()
            .statusCode(200);
    }
    @Test public void
    shouldReturn401WhenRequestOneCourseWithoutLogin() {
        when()
            .get("/admin/courses/1")
        .then()
            .statusCode(401);
    }

    /*
     * POST: "/admin/courses/"
     */
    @Test
    public void
    shouldReturn201AndCreateOneCourseWhenProperlyLogged() {
        JsonObject course = new JsonObject();
        course.addProperty("name", "Gestão da Produção");

        Response r =
            given()
                .header("Authorization", "Bearer " + this.getToken())
                .contentType("application/json")
                .body(course.toString())
            .when()
                .post("/admin/courses/");

        /* Delete after creation */
        int id = r.jsonPath().getInt("id");
        given().header("Authorization", "Bearer " + this.getToken())
                .when().delete("/admin/courses/" + id).then().statusCode(200);

        assertEquals(201, r.statusCode());
    }
    @Test public void
    shouldReturn401WhenRequestCreateOneCourseWithoutLogin() {
        JsonObject course = new JsonObject();
        course.addProperty("name", "Gestão da Produção");

        given()
            .contentType("application/json")
            .body(course.toString())
        .when()
            .post("/admin/courses/")
        .then()
            .statusCode(401);;
    }

    @Test public void
    shouldReturn401WhenRequestDeleteOneCourseWithoutLogin() {
        when()
            .delete("/admin/courses/1")
        .then()
            .statusCode(401);;
    }

    /*
     * GET: "/admin/courses/:id/disciplines/"
     */
    @Test
    public void
    shouldReturn200AndListOfCourseDisciplinesWhenProperlyLogged() {
        given()
            .header("Authorization", "Bearer " + this.getToken())
        .when().
            get("/admin/courses/2/disciplines/")
        .then()
            .statusCode(200)
            .body("list.size()", greaterThan(0));
    }
    @Test
    public void
    shouldReturn401WhenRequestListOfCourseDisciplinesWithoutLogin() {
        when()
            .get("/admin/courses/2/disciplines/")
        .then()
            .statusCode(401);
    }

    /*
     * POST: "/admin/courses/:id/disciplines/"
     */
    @Test
    public void
    shouldReturn201AndCreateOneCourseDisciplineWhenProperlyLogged() {
        JsonObject courseDiscipline = new JsonObject();
        courseDiscipline.addProperty("disciplineId", 1);
        courseDiscipline.addProperty("semester", 2);
        courseDiscipline.addProperty("year", 2017);
        courseDiscipline.addProperty("workload", 80);

        Response r =
            given()
                .header("Authorization", "Bearer " + this.getToken())
                .contentType("application/json")
                .body(courseDiscipline.toString())
            .when()
                .post("/admin/courses/2/disciplines/");

        /* Delete after creation */
        given().header("Authorization", "Bearer " + this.getToken())
                .when().delete("/admin/courses/2/disciplines/" + courseDiscipline.get("disciplineId")).then().statusCode(200);

        assertEquals(201, r.statusCode());
    }

    @Test public void
    shouldReturn401WhenRequestCreateOneCourseDisciplineWithoutLogin() {
        JsonObject courseDiscipline = new JsonObject();
        courseDiscipline.addProperty("disciplineId", 1);
        courseDiscipline.addProperty("semester", 2);
        courseDiscipline.addProperty("year", 2017);
        courseDiscipline.addProperty("workload", 80);

        given()
            .contentType("application/json")
            .body(courseDiscipline.toString())
        .when()
            .post("/admin/courses/2/disciplines/")
        .then()
            .statusCode(401);;
    }
    @Test public void
    shouldReturn401WhenRequestDeleteOneCourseDisciplineWithoutLogin() {
        when()
            .delete("/admin/courses/2/disciplines/1")
        .then()
            .statusCode(401);
    }
}