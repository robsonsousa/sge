package integration.repositories;

import edu.fatec.sge.SpringMainApplication;
import edu.fatec.sge.models.Course;
import edu.fatec.sge.repositories.*;
import factories.CourseFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;
import utils.TestUtil;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringMainApplication.class)
@Transactional
public class CourseRepositoryTest extends TestUtil {

    @Autowired
    CourseRepository courseRepository;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void courseShouldSave() {
        Course course = courseRepository
                .save(CourseFactory.validCourse());

        Course courseSaved = courseRepository.findByName(course.getName());
        assertEquals(courseSaved.getName(), course.getName());
    }

    @Test
    public void courseShouldNotBeSavedWithDuplicates() {
        Course course = CourseFactory.validCourse();

        Course courseDuplicated = CourseFactory.validCourse();
        courseDuplicated.setName("Logística");

        TestTransaction.flagForRollback();
        courseRepository.save(CourseFactory.validCourse());
        exception.expect(DataIntegrityViolationException.class);
        courseDuplicated.setName(course.getName());
        courseRepository.save(courseDuplicated);
        TestTransaction.end();

        TestTransaction.start();
        TestTransaction.flagForRollback();
        courseDuplicated.setName("Logística");
        Course courseSaved = courseRepository.save(courseDuplicated);
        TestTransaction.end();

        assertEquals(courseSaved.getName(), courseDuplicated.getName());
    }

    @Test
    public void courseShouldBeUpdated() {
        courseRepository.save(CourseFactory.validCourse());

        Course course = courseRepository.findByName("Banco de Dados - T.I");

        course.setName("Logística - LOG");
        courseRepository.save(course);

        Course courseUpdated = courseRepository.findByName(course.getName());
        assertEquals(courseUpdated.getName(), course.getName());
    }

    @Test
    public void courseShouldBeDeleted() {
        Course course = courseRepository.save(CourseFactory.validCourse());
        courseRepository.delete(course);

        Course courseDeleted = courseRepository.findByName(course.getName());
        assertEquals(null, courseDeleted);
    }

    @Test
    public void courseShouldBeListed() {
        courseRepository.save(CourseFactory.validCourse());

        List<Course> courses = courseRepository.findAll();
        assertTrue(courses.size() > 0);
    }
}
