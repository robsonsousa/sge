package integration.repositories;

import edu.fatec.sge.SpringMainApplication;
import edu.fatec.sge.models.*;
import edu.fatec.sge.repositories.*;
import factories.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import utils.TestUtil;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringMainApplication.class)
@Transactional
public class ScheduleRepositoryTest extends TestUtil {

    @Autowired
    ScheduleRepository scheduleRepository;
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    DisciplineRepository disciplineRepository;
    @Autowired
    CourseDisciplineRepository courseDisciplineRepository;

    public Schedule persistedRelationships() {
        Course course = courseRepository.save(CourseFactory.validCourse());
        Discipline discipline = disciplineRepository.save(DisciplineFactory.validDiscipline());

        CourseDiscipline courseDiscipline = CourseDisciplineFactory.validCourseDiscipline();
        courseDiscipline.setCourse(course);
        courseDiscipline.setDiscipline(discipline);
        courseDisciplineRepository.save(courseDiscipline);

        Schedule schedule = ScheduleFactory.validSchedule();
        schedule.setCourseDiscipline(courseDiscipline);
        return schedule;
    }

    @Test
    public void scheduleShouldSave() {
        Schedule schedule = persistedRelationships();
        scheduleRepository.save(schedule);

        Schedule gradeSaved = scheduleRepository.findOne(schedule.getId());
        assertNotNull(gradeSaved);
    }

    @Test
    public void scheduleShouldBeUpdated() {
        Schedule schedule = persistedRelationships();
        scheduleRepository.save(schedule);

        schedule.setQuantity(2);
        scheduleRepository.save(schedule);

        Schedule scheduleUpdated = scheduleRepository.findOne(schedule.getId());
        assertEquals(schedule.getQuantity(), scheduleUpdated.getQuantity());
    }

    @Test
    public void scheduleShouldBeDeleted() {
        Schedule schedule = persistedRelationships();
        scheduleRepository.save(schedule);

        scheduleRepository.delete(schedule);
        Schedule scheduleDeleted = scheduleRepository.findOne(schedule.getId());
        assertEquals(null, scheduleDeleted);
    }

    @Test
    public void scheduleShouldBeListed() {
        Schedule schedule = persistedRelationships();
        scheduleRepository.save(schedule);

        List<Schedule> schedules = scheduleRepository.findAll();
        assertTrue(schedules.size() > 0);
    }
}
