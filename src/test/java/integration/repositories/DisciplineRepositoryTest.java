package integration.repositories;

import edu.fatec.sge.SpringMainApplication;
import edu.fatec.sge.models.Discipline;
import edu.fatec.sge.repositories.*;
import factories.DisciplineFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;
import utils.TestUtil;

import java.util.List;

import static junit.framework.Assert.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringMainApplication.class)
@Transactional
public class DisciplineRepositoryTest extends TestUtil {

    @Autowired
    DisciplineRepository disciplineRepository;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void disciplineShouldSave(){

        Discipline discipline = disciplineRepository.save(DisciplineFactory.validDiscipline());
        Discipline disciplineSaved = disciplineRepository.findByCode(discipline.getCode());
        assertEquals(disciplineSaved.getCode(), discipline.getCode());
    }

    @Test
    public void disciplineShouldNotBeSavedWithDuplicates(){

        Discipline discipline = DisciplineFactory.validDiscipline();

        Discipline disciplineDuplicated = DisciplineFactory.validDiscipline();
        disciplineDuplicated.setCode("estrudados"); // pq não pode haver dois registros com o campo code de mesmo valor
        disciplineDuplicated.setCode(discipline.getCode());

        TestTransaction.flagForRollback();
        disciplineRepository.save(DisciplineFactory.validDiscipline());
        try {
            disciplineRepository.save(disciplineDuplicated);
            fail("Student `code` must not save with duplicated.");
        } catch (DataIntegrityViolationException ignored) {}
        TestTransaction.end();

        TestTransaction.start();
        TestTransaction.flagForRollback();
        disciplineDuplicated.setCode("estrudados");
        Discipline disciplineSaved = disciplineRepository.save(disciplineDuplicated);
        TestTransaction.end();

        assertEquals(disciplineSaved.getCode(), disciplineDuplicated.getCode());
    }

    @Test
    public void disciplineShouldBeUpdated(){

        disciplineRepository.save(DisciplineFactory.validDiscipline());

        Discipline discipline = disciplineRepository.findByCode("ed");

        discipline.setName("Algoritmos");
        disciplineRepository.save(discipline);

        Discipline disciplineUpdated = disciplineRepository.findByName(discipline.getName());
        assertEquals(disciplineUpdated.getName(), discipline.getName());
    }

    @Test
    public void disciplineShouldBeDeleted(){

        Discipline discipline = disciplineRepository.save(DisciplineFactory.validDiscipline());
        disciplineRepository.delete(discipline);

        Discipline disciplineDeleted = disciplineRepository.findByCode(discipline.getCode());
        assertEquals(null, disciplineDeleted);
    }

    @Test
    public void disciplineShouldBeListed() {
        disciplineRepository.save(DisciplineFactory.validDiscipline());

        List<Discipline> disciplines = disciplineRepository.findAll();
        assertTrue(disciplines.size() > 0);
    }
}
